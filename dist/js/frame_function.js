(function ($) {
    "use strict";
    var CR = {};
    CR.ele = {
        win: $(window),
        dom: $(document),
        guide: $(".guide"),
        qr: $(".qrcode"),
        container: $(".container"),
        iframeWrapper: $(".iframe-wrapper"),
        unsupport: $(".unsupport")
    };
    CR.checkPlatform = function () {
        return (/(iPhone|iPod|Android|webOS|BlackBerry|IEMobile|Opera Mini)/i.test(navigator.userAgent));
    };
    CR.checkSupport = function () {
        CR.ele.unsupport.hide();
    };
    CR.makeQrCode = function () {
        var renderType;
        renderType = (!!document.createElement('canvas').getContext) ? "canvas" : "table";
        CR.ele.qr.qrcode({
            width: 120,
            height: 120,
            text: window.location.href.split("#")[0],
            render: renderType
        });

    };
    CR.toggleGuide = function () {
        setTimeout(function () {
            CR.ele.guide.fadeOut();
        }, 3500);
    };
    CR.init = (function () {
        if (!CR.checkPlatform()) {
            console.log("resize");
            CR.ele.container.css({
                height: CR.ele.win.height() - 96
            });
            CR.ele.iframeWrapper.css({
                width: (CR.ele.container.height() - 10) / 736 * 414,
                height: CR.ele.container.height() - 10
            });
            CR.checkSupport();
            CR.makeQrCode();
            CR.toggleGuide();
        }
    }());
    CR.ele.win.resize(function () {
        CR.ele.container.css({
            height: CR.ele.win.height() - 96
        });
        CR.ele.iframeWrapper.css({
            width: (CR.ele.container.height() - 10) / 736 * 414,
            height: CR.ele.container.height() - 10
        });
    });
}(window.jQuery));